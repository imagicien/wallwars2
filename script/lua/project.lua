--TODO
----Units sont tils bien saves avec le projet

-----------------------------------------------------------------------------------
-- This implementation uses the default SimpleProject and the Project extensions are 
-- used to extend the SimpleProject behavior.

-- This is the global table name used by Appkit Basic project to extend behavior
Project = Project or {}
Project.players = {}    --see Project.on_level_load_pre_flow()
Project.blocks = {}     --{player = player_id, x = pos.x, y = pos.y, unit, born}
Project.coins = {}      --{player = player_id, x = pos.x, y = pos.y}
Project.mouse_score = 0
Project.mouse_coins = {}
Project.debug_str = ""

require 'script/lua/flow_callbacks'

Project.level_names = {
	empty = "content/levels/empty"
}

GRID_SIZE = 20
PLAYER_SIZE = stingray.Vector2Box(GRID_SIZE, GRID_SIZE)
COIN_SIZE = stingray.Vector2Box(GRID_SIZE/2, GRID_SIZE/2)

-- Can provide a config for the basic project, or it will use a default if not.
local SimpleProject = require 'core/appkit/lua/simple_project'
SimpleProject.config = {
	standalone_init_level_name = Project.level_names.empty,
	camera_unit = "core/appkit/units/camera/camera",
	camera_index = 1,
	shading_environment = nil, -- Will override levels that have env set in editor.
	create_free_cam_player = true, -- Project will provide its own player.
	exit_standalone_with_esc_key = true
}

-- Optional function by SimpleProject after level, world and player is loaded 
-- but before lua trigger level loaded node is activated.
function Project.on_level_load_pre_flow()
    Project.world = Appkit.managed_world.world
    
    win_x, win_y = stingray.Application.back_buffer_size()
    Project.gui = stingray.World.create_screen_gui(Project.world, win_x / 2, win_y / 2, "immediate")
    
    Project.camera_unit = Project.world:unit_by_name("camera")
    Project.mouse_player = Project.world:unit_by_name("mouse_player")
    Project.camera_rot = stingray.QuaternionBox(stingray.Quaternion.look(stingray.Vector3(0,-1,0)))
    
    local p1 = {
        position = {x=0,y=0},
        color = {255,0,0},
        keys = {"left", "right", "up", "down", "right ctrl"},
        unit_name = "core/units/primitives/cube_primitive",
        score = 0,
        name = "Red"
        }
    p1.unit = Appkit.managed_world.world:spawn_unit(p1.unit_name, stingray.Vector3(0,0,0))
    local p2 = {
        position = {x=2,y=0},
        color = {0,0,255},
        keys = {"a", "d", "w", "s", "left ctrl"},
        unit_name = "core/units/primitives/cube_primitive",
        score = 0,
        name = "Blue"
        }
    p2.unit = Appkit.managed_world.world:spawn_unit(p2.unit_name, stingray.Vector3(2,0,0))
    Project.players = {p1, p2}
    
    --Generate coins
    local x_limits = {min = -30, max = 30}
    local y_limits = {min = -15, max = 15}
    for i=1,5 do
        table.insert(Project.coins, {player = 1, x = math.random(x_limits.min, x_limits.max), y = math.random(y_limits.min, y_limits.max)})
        table.insert(Project.coins, {player = 2, x = math.random(x_limits.min, x_limits.max), y = math.random(y_limits.min, y_limits.max)})
        
        local mouse_coin = {x = math.random(x_limits.min, x_limits.max), y = math.random(y_limits.min, y_limits.max)}
        mouse_coin.unit = Appkit.managed_world.world:spawn_unit("core/units/primitives/cone_primitive", stingray.Vector3(mouse_coin.x,mouse_coin.y,0))
        table.insert(Project.mouse_coins, mouse_coin)
    end
    
    --Must be after player creation to activate keys
    Project.init_keys()
end

-- Optional function by SimpleProject after loading of level, world and player and 
-- triggering of the level loaded flow node.
function Project.on_level_shutdown_post_flow()
end

-- Optional function called by SimpleProject after world update (we will probably want to split to pre/post appkit calls)
function Project.update(dt)
    Project.update_keys()
    Project.update_players()
    Project.update_mouse_player()
    Project.update_camera()
    Project.update_blocks()
end

-- Optional function called by SimpleProject *before* appkit/world render
function Project.render()
    -- Draw blocks
    for _, block in ipairs(Project.blocks) do
        Project.draw_block(block)
    end
    
    -- Draw coins
    for _, coin in ipairs(Project.coins) do
        Project.draw_coin(coin)
    end
    
    -- Draw players
    for _, player in pairs(Project.players) do
        Project.draw_player(player)
    end
    
    -- Draw mouse player
    local mouse_player_pos = stingray.Unit.local_position(Project.mouse_player, 1)
    Project.draw_square(mouse_player_pos, {0, 255, 0})
        
    -- Draw score
    local score_text = Project.players[1].name .. ": " .. Project.players[1].score .. " -- " .. Project.players[2].name .. ": " .. Project.players[2].score .. " -- Green: " .. Project.mouse_score
    stingray.Gui.text(Project.gui, score_text, "core/performance_hud/debug", 32, "core/performance_hud/debug", stingray.Vector2(0,200), stingray.Color(0,0,0))

    -- Draw debug
    stingray.Gui.text(Project.gui, Project.debug_str, "core/performance_hud/debug", 16, "core/performance_hud/debug", stingray.Vector2(0,300), stingray.Color(0,0,0))
end

-- Optional function called by SimpleProject *before* appkit/level/player/world shutdown
function Project.shutdown()
end

--------------------------------------------------

function Project.init_keys()
    Project.keys = {}
    --We add the keys that we want to be updated
    for _, player in pairs(Project.players) do
        for _, key in pairs(player.keys) do
            Project.keys[key] = 0
        end
    end
end

function Project.update_keys()
    for key, state in pairs(Project.keys) do
        local curstate = stingray.Keyboard.button(stingray.Keyboard.button_id(key)) ~= 0
        
        if curstate == true then
            if state == 0 then
                Project.keys[key] = 1
            elseif state == 1 then
                Project.keys[key] = 2
            end
        elseif curstate == false then
            Project.keys[key] = 0
        end
    end
end

function Project.is_key_down(key)
    return stingray.Keyboard.button(stingray.Keyboard.button_id(key)) ~= 0
end

function Project.draw_square(position, color)
    local pos = stingray.Vector3(position.x * GRID_SIZE, position.y * GRID_SIZE, 0)
    local size = PLAYER_SIZE:unbox()
    stingray.Gui.rect_3d(Project.gui, stingray.Matrix4x4.identity(), pos, 1, size, convert_color4(color))
end

function Project.draw_coin(coin)
    local pos = stingray.Vector3(coin.x * GRID_SIZE, coin.y * GRID_SIZE, 0)
    stingray.Gui.rect_3d(Project.gui, stingray.Matrix4x4.identity(), pos, 1, PLAYER_SIZE:unbox(), convert_color4({255,255,0}))
    local pos2 = stingray.Vector3((coin.x + 0.25) * GRID_SIZE, (coin.y + 0.25) * GRID_SIZE, 0)
    stingray.Gui.rect_3d(Project.gui, stingray.Matrix4x4.identity(), pos2, 1, COIN_SIZE:unbox(), convert_color4(Project.players[coin.player].color))
end

function Project.draw_player(player)
    Project.draw_square(player.position, player.color)
end

function Project.draw_block(block)
    Project.draw_square({x = block.x, y = block.y}, Project.players[block.player].color)
end

function Project.update_players()
    for player_id, player in pairs(Project.players) do
        local pos = player.position
        local do_spawn = false
        
        if Project.keys[player.keys[1]] == 1 then
            pos.x = pos.x - 1
            do_spawn = true
        end
        if Project.keys[player.keys[2]] == 1 then
            pos.x = pos.x + 1
            do_spawn = true
        end
        if Project.keys[player.keys[3]] == 1 then
            pos.y = pos.y + 1
            do_spawn = true
        end
        if Project.keys[player.keys[4]] == 1 then
            pos.y = pos.y - 1
            do_spawn = true
        end
        if Project.keys[player.keys[5]] == 1 or (do_spawn and Project.is_key_down(player.keys[5])) then
            local block_unit = Appkit.managed_world.world:spawn_unit(player.unit_name, stingray.Vector3(pos.x,pos.y,0))
            table.insert(Project.blocks, {player = player_id, x = pos.x, y = pos.y, unit = block_unit, alive = true, born = os.time()})
        end
        
        Project.players[player_id].position = pos
        stingray.Unit.set_local_position(Project.players[player_id].unit, 1, stingray.Vector3(pos.x, pos.y, 0))
        stingray.Actor.teleport_position(stingray.Unit.actor(Project.players[player_id].unit,1),stingray.Vector3(pos.x, pos.y, 0))
    end
end

function Project.update_mouse_player()
    local direction = 0
    local IMPULSE_FORCE = -0.2
    if stingray.Mouse.button(stingray.Mouse.button_id("left")) == 1 then
        direction = 1
    elseif stingray.Mouse.button(stingray.Mouse.button_id("right")) == 1 then
        direction = -1
    end
    
    local cam_rot = Project.camera_rot:unbox()
    _, _, z_angle = stingray.Quaternion.to_euler_angles_xyz(cam_rot)
    local impulse_rot = stingray.Quaternion.axis_angle(stingray.Vector3(0,0,1), math.rad(z_angle))
    local impulse_vector = stingray.Quaternion.forward(impulse_rot) * IMPULSE_FORCE * direction
    stingray.Actor.add_impulse(stingray.Unit.actor(Project.mouse_player, 1), impulse_vector)
    
    --reset rotation
    stingray.Actor.teleport_rotation(stingray.Unit.actor(Project.mouse_player, 1), stingray.Quaternion.identity())
        
    --collect coins
    for i, coin in ipairs(Project.coins) do
        if stingray.Vector3.distance(stingray.Unit.local_position(Project.mouse_player,1), stingray.Vector3(coin.x, coin.y, 0)) < 0.5 then
            Project.players[coin.player].score = Project.players[coin.player].score + 1
            Project.check_win()
            Project.coins[i].player = -1
        end
    end
    --remove collected coins
    local old_coins = Project.coins
    Project.coins = {}
    for _, coin in ipairs(old_coins) do
        if coin.player ~= -1 then
            table.insert(Project.coins, coin)
        end
    end
    
    Project.update_mouse_coins()
end

function Project.update_mouse_coins()
    --collect coins
    for i, coin in ipairs(Project.mouse_coins) do
        if stingray.Vector3.distance(stingray.Unit.local_position(Project.mouse_player,1), stingray.Vector3(coin.x, coin.y, 0)) < 1 then
            Project.mouse_score = Project.mouse_score + 1
            Project.check_win()
            stingray.World.destroy_unit(Project.world, coin.unit)
            Project.mouse_coins[i].unit = nil
        end
    end
    --remove collected coins
    local old_coins = Project.mouse_coins
    Project.mouse_coins = {}
    for _, coin in ipairs(old_coins) do
        if coin.unit ~= nil then
            table.insert(Project.mouse_coins, coin)
        end
    end
end

function Project.update_blocks()
    local BLOCK_LIFETIME = 20
    for i, block in ipairs(Project.blocks) do
        if os.time() - block.born > BLOCK_LIFETIME then
            Project.blocks[i].player = -1
            stingray.World.destroy_unit(Project.world, block.unit)
        end
    end
    local old_blocks = Project.blocks
    Project.blocks = {}
    for _, block in ipairs(old_blocks) do
        if block.player ~= -1 then
            table.insert(Project.blocks, block)
        end
    end
end

function Project.update_camera()
    -- Calculate camera rotation
    local mouse_delta = stingray.Mouse.axis(stingray.Mouse.axis_id("mouse"))
    local rot_delta = stingray.Quaternion.axis_angle(stingray.Vector3(0,0,1), -mouse_delta.x / 300.0)
    local new_rot = stingray.Quaternion.multiply(Project.camera_rot:unbox(), rot_delta)
    Project.camera_rot:store(new_rot)
   
    -- Position
    _, _, z_angle = stingray.Quaternion.to_euler_angles_xyz(new_rot)
    local mouse_player_pos = stingray.Unit.local_position(Project.mouse_player, 1)
    local rev_angle = math.rad(z_angle) + math.pi/2.0
    local pos_x = mouse_player_pos.x + 5.0*math.cos(rev_angle)
    local pos_y = mouse_player_pos.y + 5.0*math.sin(rev_angle)
    local new_cam_pos = stingray.Vector3(pos_x, pos_y, mouse_player_pos.z + 2)
    stingray.Unit.set_local_position(Project.camera_unit, 1, new_cam_pos)
    
    -- Rotation
    local direction = mouse_player_pos - stingray.Unit.local_position(Project.camera_unit, 1)
    local new_rot = stingray.Quaternion.look(direction)
    stingray.Unit.set_local_rotation(Project.camera_unit, 1, new_rot)
end

function Project.check_win()
    local msg = nil
    if Project.mouse_score == 5 then
        msg = "GREEN WINS!!!"
    elseif Project.players[1].score == 5 then
        msg = "RED WINS!!!"
    elseif Project.players[2].score == 5 then
        msg = "BLUE WINS!!!"
    end
    if msg then
        print(msg)
        table.insert(nil, nil) --That's all folks!
    end
end

function convert_color(table)
    return stingray.Color(table[1], table[2], table[3])
end

function convert_color4(table)
    return stingray.Color(255, table[1], table[2], table[3])
end

return Project